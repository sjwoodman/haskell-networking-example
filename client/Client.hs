import Network
import Control.Concurrent
import System.IO

main :: IO ()
main = withSocketsDo $ do
        -- Sleep 5 seconds to ensure the server is up
         threadDelay (5 * 1000 * 1000)
         handle <- connectTo "haskellserver" (PortNumber 9001)
         hPutStr handle "Hello, world!"
         hClose handle
         main --  Recurse forever
